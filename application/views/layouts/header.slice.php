<!-- NAVBAR -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <!-- LOGO -->
        <a class="navbar-brand" href="#">Saudagar Teknologi Indonesia</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <!-- END LOGO -->
        <!-- MENU -->
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Home
                    <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">About</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Services</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Contact</a>
                </li>
            </ul>
        </div>
        <!-- END MENU -->
    </div>
</nav>
<!-- END NAVBAR -->
<!-- BANNER -->
<header class="business-header">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
            <h1 class="display-3 text-center text-white mt-4">Business Name or Tagline</h1>
            </div>
        </div>
    </div>
</header>
<!-- END BANNER -->