<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- HEADER -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
<!-- <<<<<<< HEAD -->
        <title>{{$title}}</title>
        <link href="https://blackrockdigital.github.io/startbootstrap-business-frontpage/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="https://blackrockdigital.github.io/startbootstrap-business-frontpage/css/business-frontpage.css" rel="stylesheet">
        <!-- END HEADER -->
    </head>
    <body>
        <!-- HEADER -->
        @include('layouts.header')
        <!-- END HEADER -->

        <!-- CONTENT -->
        <div class="container">
        @yield('content')
        @yield('cardtitle')
        </div>
        <!-- END CONTENT -->
        
        <!-- FOOTER -->
        @include('layouts.footer')
        <!-- END FOOTER -->
        <!-- SCRIPTS -->
        <script src="https://blackrockdigital.github.io/startbootstrap-business-frontpage/vendor/jquery/jquery.min.js"></script>
        <script src="https://blackrockdigital.github.io/startbootstrap-business-frontpage/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- END SCRIPTS -->
    </body>
</html>
