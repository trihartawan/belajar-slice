<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Landing extends CI_Controller {
	
	/** Constructor */
	public function __construct(){
		parent::__construct();
		$this->load->library('slice');
		$this->load->helper('url');
	}

	// halaman index
	public function index(){
		$data['title'] = "Halaman Landing";
		$this->slice->view('modules.landing', $data);
	}
}
